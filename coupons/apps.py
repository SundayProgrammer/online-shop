from django.apps import AppConfig


class CuponsConfig(AppConfig):
    name = 'coupons'
