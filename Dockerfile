FROM ubuntu:18.04

# locales required for update-locale
RUN apt-get update
RUN apt-get install -y software-properties-common
RUN apt-get install -y locales
RUN apt-get install -y zlib1g-dev
RUN apt-get install -y python3-pip
# installation of RabbitMQ from Package-Cloud
RUN apt-get install -y curl
RUN curl -s https://packagecloud.io/install/repositories/rabbitmq/rabbitmq-server/script.deb.sh | bash
RUN printf "deb http://dl.bintray.com/rabbitmq-erlang/debian bionic erlang-21.x" > /etc/apt/sources.list.d/bintray.rabbitmq.list
RUN curl -fsSL https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc | apt-key add -
RUN apt-get update -y
RUN apt-get install -y erlang-base \
                        erlang-asn1 erlang-crypto erlang-eldap erlang-ftp erlang-inets \
                        erlang-mnesia erlang-os-mon erlang-parsetools erlang-public-key \
                        erlang-runtime-tools erlang-snmp erlang-ssl \
                        erlang-syntax-tools erlang-tftp erlang-tools erlang-xmerl
RUN apt-get install -y rabbitmq-server --fix-missing
# install dependencies for weasyprint
RUN apt-get install -y build-essential
RUN apt-get install -y python3-dev
RUN apt-get install -y python3-pip
RUN apt-get install -y python3-setuptools
RUN apt-get install -y python3-wheel
RUN apt-get install -y python3-cffi
RUN apt-get install -y libcairo2
RUN apt-get install -y libpango-1.0-0
RUN apt-get install -y libpangocairo-1.0-0
RUN apt-get install -y libgdk-pixbuf2.0-0
RUN apt-get install -y libffi-dev
RUN apt-get install -y shared-mime-info
RUN apt-get install -y libcairo2-dev


RUN locale-gen en_US.UTF-8
RUN update-locale LANG=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8

ENV PYTHONUNBUFFERED 1

WORKDIR /home/online_shop/

RUN service rabbitmq-server start

COPY requirements.txt .
RUN python3 --version
RUN pip3 install -r /home/online_shop/requirements.txt

CMD ["/bin/bash"]