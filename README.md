# Online Shop

Simple application for online shop.

## Shop functionality

Shop components:
* product view
* shopping cart
  * persisted in the session


## Development with Docker

Accessing docker console:
```
docker exec -ti online_shop_online_shop_1 /bin/bash
```

Sample commands on docker:
```
docker-compose run online_shop sh -c "python3 manage.py runserver"
docker-compose run online_shop sh -c "python3 manage.py makemigrations"
docker-compose run online_shop sh -c "python3 manage.py migrate"
docker-compose run online_shop sh -c "python3 manage.py collectstatic"
docker-compose run online_shop sh -c "python3 manage.py createsuperuser admin"
docker-compose run online_shop sh -c "python3 -m pip install [package-name]"
docker-compose run online_shop sh -c "python3 manage.py shell"
```

## Celery

Developer tools:
* Celery Flower