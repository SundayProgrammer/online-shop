from celery import task
from django.core.mail import send_mail

from orders.models import Order

@task
def order_created(order_id):
    order = Order.objects.get(id=order_id)
    subject = 'Order nr. %s' % order.id
    message = 'Dear %s, \n\nYou have successfully placed an order. Your order id is %s' % (order.first_name,
                                                                                           order.id)
    mail_sent = send_mail(subject, message, 'admin@online_shop.com', [order.email])
    return mail_sent
